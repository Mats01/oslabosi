#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/wait.h>

int Id;

int *a = 0, procesi, iteracije;

void Proces(int i)
{
	int j;

	for (j = 1; j <= iteracije; j++)
	{
		*a = *a + 1;
	}
}

int main(int argc, char *argv[])
{

	if (argc != 3)
	{
		std::cout << "Koristenje: ./a.out BROJ_PROCESA BROJ_ITERACIJA \n";
		exit(0);
	}

	Id = shmget(IPC_PRIVATE, sizeof(int), 0600);

	if (Id == -1)
	{
		std::cout << "jako lose \n";
		exit(1);
	}

	a = (int *)shmat(Id, NULL, 0);

	*a = 0;

	procesi = atoi(argv[1]);
	iteracije = atoi(argv[2]);

	int i, pid;

	for (i = 0; i < procesi; i++)
	{
		switch (pid = fork())
		{
		case -1:
			printf("R: Ne mogu stvoriti novi proces!\n");
			exit(1);

		case 0:
			Proces(i);
			exit(0);

		default:
			break;
		}
	}

	for (i = 0; i < procesi; i++)
	{
		wait(NULL);
	}

	printf("A = %d\n", *a);

	return 0;
}