#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void *dretva(void *rbr)
{
    int i, *d = (int *)rbr;
    for (i = 1; i <= 5; i++)
    {
        printf("Dretva %d, i=%d\n", *d, i);
        sleep(1);
    }
    return NULL;
}
int main()
{
    int i, j, BR[3];
    pthread_t t[3];

    for (i = 0; i < 3; i++)
    {
        BR[i] = i;
        if (pthread_create(&t[i], NULL, dretva, &BR[i]))
        {
            printf("Ne mogu stvoriti novu dretvu!\n");
            exit(1);
        }
    }
    for (j = 0; j < 3; j++)
        pthread_join(t[j], NULL); //cekaj kraj dretve t[j] ˇ
    return 0;
}