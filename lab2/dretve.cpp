#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

int dretve, iteracije, a = 0;

void *dretva(void *brojDretve)
{
	int *d = (int *)brojDretve;
	for (int i = 1; i <= iteracije; i++)
	{
		a++;
	}
	return NULL;
}

int main(int argc, char *argv[])
{

	if (argc != 3)
	{
		std::cout << "Koristenje: ./a.out BROJ_DRETVI BROJ_ITERACIJA \n";
		exit(0);
	}

	dretve = atoi(argv[1]);
	iteracije = atoi(argv[2]);

	pthread_t t[dretve];
	int ID[dretve];

	for (int i = 0; i < dretve; i++)
	{
		ID[i] = i;
		if (pthread_create(&t[i], NULL, dretva, &ID[i]))
		{
			printf("Doslo je do greske u stvaranju dretve\n");
			exit(1);
		}
	}

	for (int j = 0; j < dretve; j++)
		pthread_join(t[j], NULL); 

	std::cout << "A = " << a << "\n";
	return 0;
}