// Uz pomoć Lamportovog algoritma moguće je sinkronizirati dvije ili više dretvi

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <atomic>
#include <cmath>

std::atomic_int *ULAZ, *BROJ;

int dretve, iteracije, a = 0;

void go_to_critical(int i)
{
	ULAZ[i] = 1;
	BROJ[i] = 1;
	int maksimalni;
	for (int k = 0; k < dretve; k++)
	{
		maksimalni = BROJ[k] + 1;
		if (maksimalni > BROJ[i])
		{
			BROJ[i] = maksimalni;
		}
	}
	ULAZ[i] = 0;

	for (int j = 0; j < dretve; j++)
	{
		while (ULAZ[j] != 0)
		{
		}
		while (BROJ[j] != 0 && (BROJ[j] < BROJ[i] || (BROJ[j] == BROJ[i] && j < i)))
		{
		}
	}
	return;
}

void *dretva(void *brd)
{
	int *d = (int *)brd;
	for (int i = 1; i <= iteracije; i++)
	{
		go_to_critical(*d);

		a = a + 1;
		BROJ[*d] = 0;
	}
	return NULL;
}

int main(int argc, char *argv[])
{

	if (argc != 3)
	{
		std::cout << "Koristenje: ./a.out BROJ_DRETVI BROJ_ITERACIJA \n";
		exit(0);
	}

	dretve = atoi(argv[1]);
	iteracije = atoi(argv[2]);

	BROJ = new std::atomic_int[dretve];
	ULAZ = new std::atomic_int[dretve];

	pthread_t t[dretve];
	int ID[dretve];

	for (int i = 0; i < dretve; i++)
	{
		ID[i] = i;
		if (pthread_create(&t[i], NULL, dretva, &ID[i]))
		{
			printf("Doslo je do greske u stvaranju dretve\n");
			exit(1);
		}
	}

	for (int j = 0; j < dretve; j++)
		pthread_join(t[j], NULL);

	std::cout << "A = " << a << "\n";
	return 0;
}