#include <iostream>
#include <stdio.h>  /* printf, scanf, puts, NULL */
#include <stdlib.h> /* srand, rand */
#include <time.h>   /* time */
#include <string>
#include <unistd.h>
#include <cmath>
#include <pthread.h>

pthread_mutex_t monitor;
pthread_cond_t red_monitora[3];

bool u_sobi_je_partibrejker, pretty_animations;
int broj_studentata_u_sobi, dretve, broj_studentata_u_sustavu;

void partyPrinter()
{

    std::cout << "\r                                                                " << std::flush;
    usleep(100 * 1000);

    int count = rand() % 25 + 10;
    std::string thing = "\rparty!";

    for (int i = 0; i < count; i++)
    {
        thing += "!";
        std::cout << thing << std::flush;
        usleep(20 * 1000);
    }

    usleep(100 * 1000);
    std::cout << "\r                                                                " << std::flush;
}

void partibrejkerPrinter()
{

    std::cout << "\rpartibrejker.  " << std::flush;
    usleep(400000);

    std::cout << "\rpartibrejker.. " << std::flush;
    usleep(400000);

    std::cout << "\rpartibrejker..." << std::flush;
    usleep(400000);

    std::cout << "\rpartibrejker.  " << std::flush;
    usleep(400000);

    std::cout << "\rpartibrejker.. " << std::flush;
    usleep(400000);

    std::cout << "\rpartibrejker..." << std::flush;
    usleep(400000);
}

void *student(void *K)
{

    int *student_id = (int *)K;
    unsigned int microseconds = (rand() % 500 + 100) * 1000; // spavaj od 100 do 500 milieskundi
    usleep(microseconds);

    for (int i = 3; i > 0; i--)
    { // ponavljaj 3 puta
        pthread_mutex_lock(&monitor);

        while (u_sobi_je_partibrejker)
        { // cekaj da u sobi ne mude partybrejkera
            pthread_cond_wait(&red_monitora[0], &monitor);
        }

        broj_studentata_u_sobi += 1;
        std::cout << "\rStudent " << *student_id << " je usao u sobu\n";
        if (pretty_animations)
        {
            partyPrinter();
            partyPrinter();
        }
        pthread_cond_signal(&red_monitora[1]);

        pthread_mutex_unlock(&monitor);
        microseconds = (rand() % 1900 + 2000) * 100; // spavaj od 100 do 500 milieskundi
        for (int i = 10; i > 0; i--)
        {
            // partyPrinter();
            // std::cout << "\r                                                                " << std::flush;
            usleep(microseconds);
        }

        pthread_mutex_lock(&monitor);

        broj_studentata_u_sobi -= 1;

        pthread_cond_signal(&red_monitora[2]);

        pthread_mutex_unlock(&monitor);

        std::cout << "\rStudent " << *student_id << " je izasao iz sobe\n";
        if (u_sobi_je_partibrejker && pretty_animations)
            // partibrejkerPrinter();

        microseconds = (rand() % 500 + 100) * 1000; // spavaj od 100 do 500 milieskundi
        usleep(microseconds);
    }
    pthread_mutex_lock(&monitor);

    std::cout << "\rStudent " << *student_id << " je gotov i ide ucit OS\n";

    broj_studentata_u_sustavu -= 1;

    pthread_cond_signal(&red_monitora[1]);

    pthread_mutex_unlock(&monitor);

    return NULL;
}

void *partibrejker(void *necutekoristit)
{
    while (broj_studentata_u_sustavu > 0)
    {
        unsigned int microseconds = (rand() % 500 + 100) * 1000; // spavaj od 100 do 500 milieskundi
        usleep(microseconds);

        pthread_mutex_lock(&monitor);

        while (broj_studentata_u_sobi < 3 && broj_studentata_u_sustavu > 0)
        { // cekaj da u sobi ne mude partybrejkera

            pthread_cond_wait(&red_monitora[1], &monitor);
        }

        std::cout << "\rPartibrejker je usao u sobu\n";

        u_sobi_je_partibrejker = true;

        pthread_mutex_unlock(&monitor);
        if (pretty_animations)
        {
            partibrejkerPrinter();
            partibrejkerPrinter();
            partibrejkerPrinter();
            partibrejkerPrinter();
        }
        pthread_mutex_lock(&monitor);

        while (broj_studentata_u_sobi > 0)
        {
            pthread_cond_wait(&red_monitora[2], &monitor);
        }


        u_sobi_je_partibrejker = false;
        std::cout << "\rPartibrejker je izasao iz sobe\n";

        pthread_cond_broadcast(&red_monitora[0]);

        pthread_mutex_unlock(&monitor);
    }

    return NULL;
}

int main(int argc, char const *argv[])
{
    if (argc != 2)
    {
        std::cout << "Koristenje: ./a.out BROJ_STUDENATA (> 3) \n";
        exit(0);
    }

    // PAZNJA: DOADO SAM ZABAVNIJI ISPIS,
    // ALI OVODIJE GA MOZETE ISKLJUCITI
    // promjeniti pretty_animations u false za iskljuciti dodatne ispise
    pretty_animations = true;
    // !!!!

    srand((unsigned int)time(NULL)); // sijanje slucajnih brojeva
    broj_studentata_u_sobi = 0;
    u_sobi_je_partibrejker = false; // inicijalizacija varijabli
    dretve = atoi(argv[1]);

    broj_studentata_u_sustavu = dretve;

    pthread_t t[dretve];
    int ID[dretve];

    pthread_mutex_init(&monitor, NULL);
    pthread_cond_init(&red_monitora[0], NULL);
    pthread_cond_init(&red_monitora[1], NULL);
    pthread_cond_init(&red_monitora[2], NULL);

    for (int i = 0; i < dretve; i++)
    {
        ID[i] = i;
        if (pthread_create(&t[i], NULL, student, &ID[i]))
        {
            printf("Doslo je do greske u stvaranju dretve\n");
            exit(1);
        }
    }
    if (pthread_create(&t[dretve], NULL, partibrejker, NULL))
    {
        printf("Doslo je do greske u stvaranju dretve\n");
        exit(1);
    }

    for (int j = 0; j < dretve + 1; j++)
        pthread_join(t[j], NULL);

    pthread_mutex_destroy(&monitor);
    pthread_cond_destroy(&red_monitora[0]);
    pthread_cond_destroy(&red_monitora[1]);
    pthread_cond_destroy(&red_monitora[2]);
    return 0;
}
