#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <string>

int Id;
sem_t *KO, *stol_prazan;
sem_t *semafori[3];
int *vrsta;

std::string stvari[] = {"papir", "duhan", "sibice"};
std::string combo[] = {"duhan i sibice", "papir i sibice", "papir i duhan"};

void Trgovac()
{
    while (1)
    {
        sem_wait(KO);

        int sastojak = rand() % 3;
        *vrsta = sastojak;
        std::cout << "Trgovac stavlja: " << combo[sastojak] << "\n";

        sem_post(KO);

        sem_post(semafori[0]);
        sem_post(semafori[1]);
        sem_post(semafori[2]);
        sem_wait(stol_prazan);
    }
}

void Pusac(int i)
{
    int trebam = i % 3;

    while (1)
    {

        sem_wait(semafori[i]);

        sem_wait(KO);

        if (trebam == *vrsta)
        {
            std::cout << "Pusac " << i << " uzima sastojke i...\n";

            //uzmi frikin sastojke
            *vrsta = -1;
            sem_post(KO);
            sem_post(stol_prazan);

            //trosi_viijeme
            std::string cigara = " ###||||||||||||||||||||\n\n";
           
            std::cout << "cigareta od "<< i << cigara;


            sleep(1);
        }
        else
        {

            sem_post(KO);
        }
    }
}

int main(int argc, char *argv[])
{
    srand(time(0));

    if (Id == -1)
    {
        std::cout << "jako lose \n";
        exit(1);
    }

    int i, pid;

    int ID = shmget(IPC_PRIVATE, (sizeof(sem_t) * 5) + sizeof(int), 0600);
    if (ID == -1)
    {
        std::cout << "jako lose \n";
        exit(1);
    }

    KO = (sem_t *)shmat(ID, NULL, 0);
    stol_prazan = (sem_t *)(KO + 1);
    semafori[0] = (sem_t *)(KO + 2);
    semafori[1] = (sem_t *)(KO + 3);
    semafori[2] = (sem_t *)(KO + 4);
    vrsta = (int *)(KO + 5);
    shmctl(ID, IPC_RMID, NULL); //moze odmah ovdje, nakon shmat

    sem_init(KO, 1, 1);
    sem_init(stol_prazan, 1, 1);
    sem_init(semafori[0], 1, 0);
    sem_init(semafori[1], 1, 0);
    sem_init(semafori[2], 1, 0);

    int id[4];

    for (i = 0; i < 3; i++)
    {
        std::cout << "Pusac " << i << ": ima " << stvari[i] << "\n";

        switch (pid = fork())
        {
        case -1:
            printf("R: Ne mogu stvoriti novi proces!\n");
            exit(1);

        case 0:
            id[i] = i;
            Pusac(id[i]);

            exit(0);

        default:
            break;
        }
    }

    switch (pid = fork())
    {
    case -1:
        printf("R: Ne mogu stvoriti novi proces!\n");
        exit(1);

    case 0:
        Trgovac();
        exit(0);

    default:
        break;
    }

    for (i = 0; i < 3; i++)
    {
        wait(NULL);
    }

    return 0;
}