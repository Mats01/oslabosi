import random
import time
import pprint
from bisect import insort


class ToNijeDretvaException(Exception):
    pass


def ima_dretvi():
    global red_dretvi
    global aktivna_dretva

    broj_dretvi = 0
    if aktivna_dretva:
        broj_dretvi += 1

    for i in red_dretvi:
        for dretva in i:
            broj_dretvi += 1
    return broj_dretvi


def aktiviraj_slijedecu_dretvu():
    global aktivna_dretva

    for i in range(MAX_DRETVI):
        if(red_dretvi[i]):
            aktivna_dretva = red_dretvi[i].pop(0)
            if not isinstance(aktivna_dretva, Dretva):
                raise ToNijeDretvaException(
                    "jao, pokusao smo aktivirati nesto sto nije dretva")
            return True
    else:
        return False


def aktiviraj_ili_pospremi(dretva):
    global aktivna_dretva
    if not isinstance(dretva, Dretva):
        raise ToNijeDretvaException("predali ste nesto sto nije dretva")

    if not aktivna_dretva:
        aktivna_dretva = dretva
        return

    if dretva.prioritet < aktivna_dretva.prioritet:  # maji prioritet je vazniji
        aktivna_dretva.pospremi_se()
        aktivna_dretva = dretva

    else:
        dretva.pospremi_se()


def obradi_aktivnu_dretvu():
    global aktivna_dretva

    aktivna_dretva.preostalo_vrijeme -= 1

    if aktivna_dretva.preostalo_vrijeme == 0:
        print('dretva %d je zavrsila' % aktivna_dretva.id)
        aktivna_dretva = None  # dretva je gotova i pustit cemo da je garbage collector rijesi

    elif aktivna_dretva.nacin_rasporedivanja == 1:
        aktivna_dretva.pospremi_se()
        aktivna_dretva = None


def slusaj_nove_dretve():
    global id_dretve
    global ima
    nove_dretve = []
    if ima >= BR_DRETVI:
        return nove_dretve
    if (pojavljivanje[ima] == t):

        # gereria se nova dretva s polu_slucajnim parametrima
        nove_dretve.append(Dretva(id_dretve))
        id_dretve += 1
        ima += 1

        for nova in slusaj_nove_dretve():
            if nova:
                nove_dretve.append(nova)

    return nove_dretve


def slusaj_nove_dretve_dok_ima_mjesta():
    global id_dretve
    global t
    global ima
    nove_dretve = []
    moogucih = MAX_DRETVI - ima_dretvi()

    for index, i in enumerate(pojavljivanje):
        if not moogucih:
            return nove_dretve

        if i != t:
            continue
        if i > t:
            break
        # gereria se nova dretva s polu_slucajnim parametrima
        nove_dretve.append(Dretva(id_dretve))
        id_dretve += 1
        moogucih -= 1
        if index == len(pojavljivanje) - 1:
            ima = BR_DRETVI  # javlja while petlji da smo gotovi

    return nove_dretve


def ispis_stanja():

    out = "%3d " % t
    if aktivna_dretva:
        out += str(aktivna_dretva)
    else:
        out += "  -/-/- "

    other = MAX_DRETVI-1
    for red in red_dretvi:
        for dretva in red:
            other -= 1
            out += str(dretva)

    for i in range(other):
        out += "  -/-/- "

    out += "\n"

    print(out)


class Dretva:

    def __init__(self, id, preostalo_vrijeme=1, prioritet=0, nacin_rasporedivanja=0):
        self.id = id
        self.preostalo_vrijeme = random.randint(1, 10)
        self.prioritet = random.randint(0, MAX_DRETVI-1)
        self.nacin_rasporedivanja = random.randint(0, 1)  # 0 = FIFO, 1 = RR

        print("%3d -- stvorena nova dretva s id=%d i proritetom %d" %
              (t, id, self.prioritet))

    def __str__(self):
        return "  %d/%d/%d " % (self.id, self.prioritet, self.preostalo_vrijeme)

    def __gt__(self, other):
        if not isinstance(other, Dretva):
            raise ToNijeDretvaException("predali ste nesto sto nije dretva")
        return self.prioritet > other.prioritet

    def pauziraj_se(self):
        insort(pauzirane_dretve, self)
        # pauzirane_dretve.append(self)

        print("    -- pauzirana dretva s id=%d " % self.id)

    def pospremi_se(self):
        red_dretvi[self.prioritet].append(self)

# BEGIN INICIJALIZACIJA


random.seed(time.time())
id_dretve = 0
MAX_DRETVI = 6  # toliko moze biti i raina prioriteta
t = 0
# trenuci za stvaranje novih dretvi
pojavljivanje = [1, 2, 3, 3, 3, 3, 3, 3, 6, 7, 11, 12, 13, 16, 18, 20]
BR_DRETVI = len(pojavljivanje)
ima = 0
aktivna_dretva = None
red_dretvi = []
pauzirane_dretve = []

for i in range(MAX_DRETVI):
    red_dretvi.append([])

# END INICIJALIZACIJA


# GLAVNI PROGRAM ---

first_line = "  t    AKT"
for i in range(1, MAX_DRETVI):
    first_line += "     PR%d" % i
print(first_line)

while(ima < BR_DRETVI or ima_dretvi()):

    if not aktivna_dretva:
        aktiviraj_slijedecu_dretvu()

    for i in range(len(pauzirane_dretve)):
        if ima_dretvi() == MAX_DRETVI:
            break
        aktiviraj_ili_pospremi(pauzirane_dretve.pop(0))

    # ako zelimo spremiti dretve za koje nismo imali mjesta i posluziti ih kasnije
    # koristiti slusaj_nove_dretve umjesto slusaj_nove_dretve_dok_ima_mjesta

    # nove_dretve = sorted(slusaj_nove_dretve()) # ovo rijesenje slusa na efikasniji nacin
    nove_dretve = sorted(slusaj_nove_dretve_dok_ima_mjesta())

    if nove_dretve:
        for dretva in nove_dretve:
            if ima_dretvi() < MAX_DRETVI:
                aktiviraj_ili_pospremi(dretva)
            else:
                dretva.pauziraj_se()

    ispis_stanja()

    if aktivna_dretva:
        obradi_aktivnu_dretvu()

    t += 1

    time.sleep(1)

print("simulacija zavrsena")
