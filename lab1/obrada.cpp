// program koji omogućava obradu prekida s više razina/prioriteta
// (simulira ponašanje sustava opisanog u 3. poglavlju
// i to bez sklopa za prihvat prekida)

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED

#include <iostream>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <cmath>
#include <string>

#define N 6 /* broj razina proriteta */

int OZNAKA_CEKANJA[N] = {0};
int TEKUCI_PRIORITET = 0;
int PRIORITET[N] = {0};

int signali[] = {SIGUSR1, SIGUSR2, SIGTERM, SIGTSTP, SIGINT};

void zabrani_prekidanje()
{
	int i;
	for (i = 0; i < 5; i++)
		sighold(signali[i]);
}
void dozvoli_prekidanje()
{
	int i;
	for (i = 0; i < 5; i++)
		sigrelse(signali[i]);
}

void obrada_signala(int n)
{

	for (int a = 1; a < 6; a++)
	{
		
		std::string rez;
		rez = "";
		for (int i = 0; i < 6; i++)
		{
			if (i == n)
				rez += " X";
			else
				rez += " -";
		}
		std::cout << rez << " (" << a << "/5) oznaka_cekanja["
				  << OZNAKA_CEKANJA[0] << ", "
				  << OZNAKA_CEKANJA[1] << ", "
				  << OZNAKA_CEKANJA[2] << ", "
				  << OZNAKA_CEKANJA[3] << ", "
				  << OZNAKA_CEKANJA[4] << ", "
				  << OZNAKA_CEKANJA[5] << "]"
				  << "\n"; 

		sleep(1);
	}

}

void prekidna_rutina(int sig)
{
	int n = 0;
	zabrani_prekidanje();
	switch (sig)
	{
	case SIGUSR1:
		n = 1;
		break;
	case SIGUSR2:
		n = 2;
		break;
	case SIGTERM:
		n = 3;
		break;
	case SIGTSTP:
		n = 4;
		break;
	case SIGINT:
		n = 5;
		break;
	}

	OZNAKA_CEKANJA[n]++; // ima jos jedan takav prekid koji ceka
	int prioritetniji;
	do{
		prioritetniji = 0;
		for(int i = 5; i >= TEKUCI_PRIORITET+1; i--){
			if(OZNAKA_CEKANJA[i] > 0){
				OZNAKA_CEKANJA[i]--;
				PRIORITET[i] = TEKUCI_PRIORITET;
				TEKUCI_PRIORITET = i;
				prioritetniji = i;
				dozvoli_prekidanje();
				obrada_signala(i);
				zabrani_prekidanje();
				TEKUCI_PRIORITET = PRIORITET[i];
				break;

			}
		}

	}while(prioritetniji > 0);

	dozvoli_prekidanje();
}

void zaustavinator(int sig){
	printf("killed\n");
	exit(0);
}

int main(void)
{
	sigset(SIGUSR1, prekidna_rutina);
	sigset(SIGUSR2, prekidna_rutina);
	sigset(SIGTERM, prekidna_rutina);
	sigset(SIGTSTP, prekidna_rutina);
	sigset(SIGTRAP, prekidna_rutina);
	sigset(SIGINT, prekidna_rutina);
	sigset(SIGKILL, zaustavinator);

	printf("Proces obrade prekida, PID=%ld\n", getpid());

	for (int a = 0; a < 30; a++)
	{
		sleep(1);

		printf(" G - - - - - (%d/30)\n", a);
	}

	printf("Zavrsio osnovni program\n");

	return 0;
}