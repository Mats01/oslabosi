#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED

#include <iostream>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <cmath>

int pauza = 0;
int broj = 1000000001;
int zadnji = 1000000001;

void periodicki_ispis(int sig)
{
	printf("Prost broj..........%d\n", zadnji);
}

void postavi_pauzu (int sig) {
	if (pauza == 0)
	printf("Pauziram..........\n");
	else
	{
		printf("Nastavljam........\n");
	}
	
	pauza = 1 - pauza;
}

void prekini(int sig)
{
	printf("ovaj je zadnji......%d\n", zadnji);
	exit (0);
}

bool prost_broj_provjerinator(int broj)
{
	for (int i = 2; i < sqrt(broj); i++)
	{
		if (broj % i == 0)
			return false;
	}

	return true;
}

int main()
{
	printf("Poceo osnovni program PID=%d\n", getpid());
	

	sigset(SIGINT, postavi_pauzu);
	sigset(SIGTERM, prekini);


	struct itimerval t;

	sigset(SIGALRM, periodicki_ispis);

	t.it_value.tv_sec = 0;
	t.it_value.tv_usec = 1; // inace odbrojavanje ne radi za cijeli broj sekundi 

	t.it_interval.tv_sec = 0;
	t.it_interval.tv_sec = 5;

	setitimer(ITIMER_REAL, &t, NULL);

	while (1)
	{
		if (prost_broj_provjerinator(broj))
		{
			zadnji = broj;
		}
		broj++;

		while (pauza == 1)
		{
			pause();
		}
	}

	return 0;
}
