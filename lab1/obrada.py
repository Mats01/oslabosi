import os
import time
import signal as s 

SIGINT = s.SIGINT

def sigset(signal, function):
    s.signal(signal, function)

def receiveSignal(signalNumber, frame):
    print('Received:', signalNumber)
    return


if __name__ == '__main__':
    sigset(SIGINT, receiveSignal)

 # output current process id
print('My PID is:', os.getpid())

# wait in an endless loop for signals 
while True:
    print('Waiting...')
    time.sleep(3)
