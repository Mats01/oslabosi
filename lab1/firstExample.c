#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED

#include <stdio.h>
#include <signal.h>
void prekidna_rutina(int sig)
{
	printf("prekid lol\n");
}
int main(void)
{
	sigset(SIGINT, prekidna_rutina);
	printf("Poceo osnovni program PID=%d\n", getpid());
	/* troši vrijeme da se ima šta prekinuti - 10 s */
	for(auto a = 0; a <10; a++) sleep(1);
	printf("Zavrsio osnovni program\n");
	return 0;
}