// program koji generira signale i šalje ih prvom procesu

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED

#include <iostream>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <cmath>
#include <stdlib.h> /* srand, rand */
#include <time.h>	/* time */

int pid = 0;

int signali[] = {SIGUSR1, SIGUSR2, SIGTERM, SIGTSTP, SIGTRAP, SIGINT};

void prekidna_rutina(int sig)
{
	kill(pid, SIGKILL);
	exit(0);
}

int odspavinator(int low, int high)
{
	double broj = (double)rand() / RAND_MAX;
	broj = (high - low) * broj + low;
	int rezultat_inatora = (int)broj;
	return rezultat_inatora;
}

int main(int argc, char *argv[])
{
	srand(time(NULL));

	pid = atoi(argv[1]);

	sigset(SIGINT, prekidna_rutina);
	while (1)
	{
		int cekanje = odspavinator(3, 5);
		sleep(cekanje);

		printf("spavao sam %d\n", cekanje);

		int indeks_signala = odspavinator(0, 4);
		printf("biram signal %d\n", signali[indeks_signala]);

		kill(pid, signali[indeks_signala]);
	}

	return 0;
}
